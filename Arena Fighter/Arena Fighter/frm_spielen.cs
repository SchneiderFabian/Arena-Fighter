﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arena_Fighter
{
    public partial class frm_spielen : Form
    {
        public frm_spielen()
        {
            InitializeComponent();
        }
        int leben1 = 7;
        int angriffe1 = 0;
        int rest = 0;
        int ausdauer1 = 1;
        int block1 = 0;
        int fertig = 0;


        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pb_angriff1_Click(object sender, EventArgs e)
        {
            if(tmr_spiel.Enabled)
            { if(ausdauer1 >= 1)
                { angriffe1 ++;
                    ausdauer1 --;
                }
                lbl_angriffe.Text = Convert.ToString(angriffe1);
                lbl_ausdauer1.Text = Convert.ToString(ausdauer1);
            }
        }

        private void pb_block_Click(object sender, EventArgs e)
        {
            if (tmr_spiel.Enabled)
            {
                if (ausdauer1 >= 1)
                {
                    block1++;
                    ausdauer1--;
                }
                lbl_blocks.Text = Convert.ToString(block1);
                lbl_ausdauer1.Text = Convert.ToString(ausdauer1);
            }
        }

        private void pb_rest_Click(object sender, EventArgs e)
        {
            if (tmr_spiel.Enabled)
            {
                if (ausdauer1 >= 1)
                {
                    rest ++;
                    ausdauer1--;
                }
                lbl_rests.Text = Convert.ToString(rest);
                lbl_ausdauer1.Text = Convert.ToString(ausdauer1);
            }
        }

        private void pb_angriff2_Click(object sender, EventArgs e)
        {

        }

        private void pb_block2_Click(object sender, EventArgs e)
        {

        }

        private void pb_rest2_Click(object sender, EventArgs e)
        {

        }

        private void tmr_spiel_Tick(object sender, EventArgs e)
        {
            if(ausdauer1 == 0)
            {
                ausdauer1 = 1;
                tmr_spiel.Enabled = false;
                fertig = 1;
                angriffe1 = 0;
                block1 = 0;
                ausdauer1 = rest + ausdauer1;
                rest = 0;


                //s1 daten (leben, angriffe, blocks, fertig) in datenbank schicken
                tmr_auslesen.Start();
                tmr_spiel.Stop();
            }
        }

        private void tmr_auslesen_Tick(object sender, EventArgs e)
        {
            //datenbank auslesen (spieler2: leben, angriffe, blocks, fertig) bis spieler2 fertig = 1
            MessageBox.Show("lese");
            //if spieler2 fertig == 1;
            fertig = 0;
            lbl_rests.Text = Convert.ToString(rest);
            lbl_ausdauer1.Text = Convert.ToString(ausdauer1);
            lbl_angriffe.Text = Convert.ToString(angriffe1);
            lbl_blocks.Text = Convert.ToString(block1);
            tmr_spiel.Start();
            tmr_auslesen.Stop();
        }

        private void frm_spielen_Load(object sender, EventArgs e)
        {

        }
    }
}
