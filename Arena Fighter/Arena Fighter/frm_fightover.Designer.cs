﻿namespace Arena_Fighter
{
    partial class frm_fightover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_kampfvorbei = new System.Windows.Forms.Label();
            this.btn_zurueckover_home = new System.Windows.Forms.Button();
            this.lbx_kampfvorbei = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbl_kampfvorbei
            // 
            this.lbl_kampfvorbei.AutoSize = true;
            this.lbl_kampfvorbei.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbl_kampfvorbei.Location = new System.Drawing.Point(230, 44);
            this.lbl_kampfvorbei.Name = "lbl_kampfvorbei";
            this.lbl_kampfvorbei.Size = new System.Drawing.Size(146, 24);
            this.lbl_kampfvorbei.TabIndex = 5;
            this.lbl_kampfvorbei.Text = "Kampf vorbei";
            // 
            // btn_zurueckover_home
            // 
            this.btn_zurueckover_home.Location = new System.Drawing.Point(129, 504);
            this.btn_zurueckover_home.Name = "btn_zurueckover_home";
            this.btn_zurueckover_home.Size = new System.Drawing.Size(345, 40);
            this.btn_zurueckover_home.TabIndex = 4;
            this.btn_zurueckover_home.Text = "Zurück";
            this.btn_zurueckover_home.UseVisualStyleBackColor = true;
            // 
            // lbx_kampfvorbei
            // 
            this.lbx_kampfvorbei.FormattingEnabled = true;
            this.lbx_kampfvorbei.Location = new System.Drawing.Point(129, 106);
            this.lbx_kampfvorbei.Name = "lbx_kampfvorbei";
            this.lbx_kampfvorbei.Size = new System.Drawing.Size(345, 368);
            this.lbx_kampfvorbei.TabIndex = 3;
            // 
            // frm_fightover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(592, 600);
            this.Controls.Add(this.lbl_kampfvorbei);
            this.Controls.Add(this.btn_zurueckover_home);
            this.Controls.Add(this.lbx_kampfvorbei);
            this.Name = "frm_fightover";
            this.Text = "Kampf vorbei";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_kampfvorbei;
        private System.Windows.Forms.Button btn_zurueckover_home;
        private System.Windows.Forms.ListBox lbx_kampfvorbei;
    }
}