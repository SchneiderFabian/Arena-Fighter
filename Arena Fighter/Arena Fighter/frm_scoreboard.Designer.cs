﻿namespace Arena_Fighter
{
    partial class frm_scoreboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbx_scoreboard = new System.Windows.Forms.ListBox();
            this.btn_zurueckscoreboard_home = new System.Windows.Forms.Button();
            this.lbl_scoreboard = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbx_scoreboard
            // 
            this.lbx_scoreboard.FormattingEnabled = true;
            this.lbx_scoreboard.Location = new System.Drawing.Point(76, 95);
            this.lbx_scoreboard.Name = "lbx_scoreboard";
            this.lbx_scoreboard.Size = new System.Drawing.Size(345, 368);
            this.lbx_scoreboard.TabIndex = 0;
            // 
            // btn_zurueckscoreboard_home
            // 
            this.btn_zurueckscoreboard_home.Location = new System.Drawing.Point(76, 493);
            this.btn_zurueckscoreboard_home.Name = "btn_zurueckscoreboard_home";
            this.btn_zurueckscoreboard_home.Size = new System.Drawing.Size(345, 40);
            this.btn_zurueckscoreboard_home.TabIndex = 1;
            this.btn_zurueckscoreboard_home.Text = "Zurück";
            this.btn_zurueckscoreboard_home.UseVisualStyleBackColor = true;
            // 
            // lbl_scoreboard
            // 
            this.lbl_scoreboard.AutoSize = true;
            this.lbl_scoreboard.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbl_scoreboard.Location = new System.Drawing.Point(183, 38);
            this.lbl_scoreboard.Name = "lbl_scoreboard";
            this.lbl_scoreboard.Size = new System.Drawing.Size(128, 24);
            this.lbl_scoreboard.TabIndex = 2;
            this.lbl_scoreboard.Text = "Scoreboard";
            this.lbl_scoreboard.Click += new System.EventHandler(this.lbl_scoreboard_Click);
            // 
            // frm_scoreboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(511, 572);
            this.Controls.Add(this.lbl_scoreboard);
            this.Controls.Add(this.btn_zurueckscoreboard_home);
            this.Controls.Add(this.lbx_scoreboard);
            this.Name = "frm_scoreboard";
            this.Text = "Scoreboard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbx_scoreboard;
        private System.Windows.Forms.Button btn_zurueckscoreboard_home;
        private System.Windows.Forms.Label lbl_scoreboard;
    }
}