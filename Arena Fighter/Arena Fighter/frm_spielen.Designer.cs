﻿namespace Arena_Fighter
{
    partial class frm_spielen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pb_lebens2 = new System.Windows.Forms.PictureBox();
            this.pb_pbs2 = new System.Windows.Forms.PictureBox();
            this.pb_pbs1 = new System.Windows.Forms.PictureBox();
            this.pb_lebens1 = new System.Windows.Forms.PictureBox();
            this.pb_angriff1 = new System.Windows.Forms.PictureBox();
            this.pb_block = new System.Windows.Forms.PictureBox();
            this.pb_rest = new System.Windows.Forms.PictureBox();
            this.lbl_ausdauer1 = new System.Windows.Forms.Label();
            this.lbl_spieler2name = new System.Windows.Forms.Label();
            this.lbl_spieler2punkte = new System.Windows.Forms.Label();
            this.lbl_spieler1punkte = new System.Windows.Forms.Label();
            this.lbl_spieler1name = new System.Windows.Forms.Label();
            this.pb_spielensprite = new System.Windows.Forms.PictureBox();
            this.tmr_spiel = new System.Windows.Forms.Timer(this.components);
            this.lbl_angriffe = new System.Windows.Forms.Label();
            this.lbl_blocks = new System.Windows.Forms.Label();
            this.lbl_rests = new System.Windows.Forms.Label();
            this.tmr_auslesen = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pb_lebens2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pbs2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pbs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_lebens1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_angriff1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_block)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_rest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_spielensprite)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_lebens2
            // 
            this.pb_lebens2.BackColor = System.Drawing.Color.Red;
            this.pb_lebens2.Location = new System.Drawing.Point(669, 14);
            this.pb_lebens2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_lebens2.Name = "pb_lebens2";
            this.pb_lebens2.Size = new System.Drawing.Size(821, 110);
            this.pb_lebens2.TabIndex = 0;
            this.pb_lebens2.TabStop = false;
            // 
            // pb_pbs2
            // 
            this.pb_pbs2.Location = new System.Drawing.Point(1499, 15);
            this.pb_pbs2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_pbs2.Name = "pb_pbs2";
            this.pb_pbs2.Size = new System.Drawing.Size(132, 108);
            this.pb_pbs2.TabIndex = 1;
            this.pb_pbs2.TabStop = false;
            // 
            // pb_pbs1
            // 
            this.pb_pbs1.Location = new System.Drawing.Point(16, 682);
            this.pb_pbs1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_pbs1.Name = "pb_pbs1";
            this.pb_pbs1.Size = new System.Drawing.Size(132, 108);
            this.pb_pbs1.TabIndex = 3;
            this.pb_pbs1.TabStop = false;
            // 
            // pb_lebens1
            // 
            this.pb_lebens1.BackColor = System.Drawing.Color.Red;
            this.pb_lebens1.Location = new System.Drawing.Point(156, 681);
            this.pb_lebens1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_lebens1.Name = "pb_lebens1";
            this.pb_lebens1.Size = new System.Drawing.Size(821, 110);
            this.pb_lebens1.TabIndex = 2;
            this.pb_lebens1.TabStop = false;
            // 
            // pb_angriff1
            // 
            this.pb_angriff1.BackColor = System.Drawing.Color.Red;
            this.pb_angriff1.Location = new System.Drawing.Point(1053, 681);
            this.pb_angriff1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_angriff1.Name = "pb_angriff1";
            this.pb_angriff1.Size = new System.Drawing.Size(132, 108);
            this.pb_angriff1.TabIndex = 4;
            this.pb_angriff1.TabStop = false;
            this.pb_angriff1.Click += new System.EventHandler(this.pb_angriff1_Click);
            // 
            // pb_block
            // 
            this.pb_block.BackColor = System.Drawing.Color.Blue;
            this.pb_block.Location = new System.Drawing.Point(1193, 682);
            this.pb_block.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_block.Name = "pb_block";
            this.pb_block.Size = new System.Drawing.Size(132, 108);
            this.pb_block.TabIndex = 5;
            this.pb_block.TabStop = false;
            this.pb_block.Click += new System.EventHandler(this.pb_block_Click);
            // 
            // pb_rest
            // 
            this.pb_rest.BackColor = System.Drawing.Color.Orange;
            this.pb_rest.Location = new System.Drawing.Point(1333, 682);
            this.pb_rest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_rest.Name = "pb_rest";
            this.pb_rest.Size = new System.Drawing.Size(132, 108);
            this.pb_rest.TabIndex = 6;
            this.pb_rest.TabStop = false;
            this.pb_rest.Click += new System.EventHandler(this.pb_rest_Click);
            // 
            // lbl_ausdauer1
            // 
            this.lbl_ausdauer1.AutoSize = true;
            this.lbl_ausdauer1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ausdauer1.Location = new System.Drawing.Point(1520, 706);
            this.lbl_ausdauer1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ausdauer1.Name = "lbl_ausdauer1";
            this.lbl_ausdauer1.Size = new System.Drawing.Size(51, 54);
            this.lbl_ausdauer1.TabIndex = 7;
            this.lbl_ausdauer1.Text = "1";
            // 
            // lbl_spieler2name
            // 
            this.lbl_spieler2name.AutoSize = true;
            this.lbl_spieler2name.Location = new System.Drawing.Point(665, 127);
            this.lbl_spieler2name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_spieler2name.Name = "lbl_spieler2name";
            this.lbl_spieler2name.Size = new System.Drawing.Size(95, 17);
            this.lbl_spieler2name.TabIndex = 8;
            this.lbl_spieler2name.Text = "Spieler2name";
            // 
            // lbl_spieler2punkte
            // 
            this.lbl_spieler2punkte.AutoSize = true;
            this.lbl_spieler2punkte.Location = new System.Drawing.Point(1387, 127);
            this.lbl_spieler2punkte.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_spieler2punkte.Name = "lbl_spieler2punkte";
            this.lbl_spieler2punkte.Size = new System.Drawing.Size(103, 17);
            this.lbl_spieler2punkte.TabIndex = 9;
            this.lbl_spieler2punkte.Text = "Spieler2punkte";
            // 
            // lbl_spieler1punkte
            // 
            this.lbl_spieler1punkte.AutoSize = true;
            this.lbl_spieler1punkte.Location = new System.Drawing.Point(873, 661);
            this.lbl_spieler1punkte.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_spieler1punkte.Name = "lbl_spieler1punkte";
            this.lbl_spieler1punkte.Size = new System.Drawing.Size(103, 17);
            this.lbl_spieler1punkte.TabIndex = 11;
            this.lbl_spieler1punkte.Text = "Spieler1punkte";
            // 
            // lbl_spieler1name
            // 
            this.lbl_spieler1name.AutoSize = true;
            this.lbl_spieler1name.Location = new System.Drawing.Point(152, 661);
            this.lbl_spieler1name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_spieler1name.Name = "lbl_spieler1name";
            this.lbl_spieler1name.Size = new System.Drawing.Size(95, 17);
            this.lbl_spieler1name.TabIndex = 10;
            this.lbl_spieler1name.Text = "Spieler1name";
            // 
            // pb_spielensprite
            // 
            this.pb_spielensprite.Location = new System.Drawing.Point(291, 14);
            this.pb_spielensprite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_spielensprite.Name = "pb_spielensprite";
            this.pb_spielensprite.Size = new System.Drawing.Size(1065, 801);
            this.pb_spielensprite.TabIndex = 12;
            this.pb_spielensprite.TabStop = false;
            // 
            // tmr_spiel
            // 
            this.tmr_spiel.Enabled = true;
            this.tmr_spiel.Tick += new System.EventHandler(this.tmr_spiel_Tick);
            // 
            // lbl_angriffe
            // 
            this.lbl_angriffe.AutoSize = true;
            this.lbl_angriffe.Location = new System.Drawing.Point(1109, 661);
            this.lbl_angriffe.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_angriffe.Name = "lbl_angriffe";
            this.lbl_angriffe.Size = new System.Drawing.Size(16, 17);
            this.lbl_angriffe.TabIndex = 17;
            this.lbl_angriffe.Text = "0";
            // 
            // lbl_blocks
            // 
            this.lbl_blocks.AutoSize = true;
            this.lbl_blocks.Location = new System.Drawing.Point(1249, 662);
            this.lbl_blocks.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_blocks.Name = "lbl_blocks";
            this.lbl_blocks.Size = new System.Drawing.Size(16, 17);
            this.lbl_blocks.TabIndex = 18;
            this.lbl_blocks.Text = "0";
            // 
            // lbl_rests
            // 
            this.lbl_rests.AutoSize = true;
            this.lbl_rests.Location = new System.Drawing.Point(1387, 662);
            this.lbl_rests.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_rests.Name = "lbl_rests";
            this.lbl_rests.Size = new System.Drawing.Size(16, 17);
            this.lbl_rests.TabIndex = 19;
            this.lbl_rests.Text = "0";
            // 
            // tmr_auslesen
            // 
            this.tmr_auslesen.Interval = 1000;
            this.tmr_auslesen.Tick += new System.EventHandler(this.tmr_auslesen_Tick);
            // 
            // frm_spielen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1647, 805);
            this.Controls.Add(this.lbl_rests);
            this.Controls.Add(this.lbl_blocks);
            this.Controls.Add(this.lbl_angriffe);
            this.Controls.Add(this.lbl_spieler1punkte);
            this.Controls.Add(this.lbl_spieler1name);
            this.Controls.Add(this.lbl_spieler2punkte);
            this.Controls.Add(this.lbl_spieler2name);
            this.Controls.Add(this.lbl_ausdauer1);
            this.Controls.Add(this.pb_rest);
            this.Controls.Add(this.pb_block);
            this.Controls.Add(this.pb_angriff1);
            this.Controls.Add(this.pb_pbs1);
            this.Controls.Add(this.pb_lebens1);
            this.Controls.Add(this.pb_pbs2);
            this.Controls.Add(this.pb_lebens2);
            this.Controls.Add(this.pb_spielensprite);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frm_spielen";
            this.Text = "Arena Fighter";
            this.Load += new System.EventHandler(this.frm_spielen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_lebens2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pbs2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pbs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_lebens1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_angriff1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_block)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_rest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_spielensprite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_lebens2;
        private System.Windows.Forms.PictureBox pb_pbs2;
        private System.Windows.Forms.PictureBox pb_pbs1;
        private System.Windows.Forms.PictureBox pb_lebens1;
        private System.Windows.Forms.PictureBox pb_angriff1;
        private System.Windows.Forms.PictureBox pb_block;
        private System.Windows.Forms.PictureBox pb_rest;
        private System.Windows.Forms.Label lbl_ausdauer1;
        private System.Windows.Forms.Label lbl_spieler2name;
        private System.Windows.Forms.Label lbl_spieler2punkte;
        private System.Windows.Forms.Label lbl_spieler1punkte;
        private System.Windows.Forms.Label lbl_spieler1name;
        private System.Windows.Forms.PictureBox pb_spielensprite;
        private System.Windows.Forms.Timer tmr_spiel;
        private System.Windows.Forms.Label lbl_angriffe;
        private System.Windows.Forms.Label lbl_blocks;
        private System.Windows.Forms.Label lbl_rests;
        private System.Windows.Forms.Timer tmr_auslesen;
    }
}