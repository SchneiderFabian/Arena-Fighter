﻿namespace Arena_Fighter
{
    partial class frm_Character
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_character = new System.Windows.Forms.PictureBox();
            this.lbl_spielername = new System.Windows.Forms.Label();
            this.lbl_punkte = new System.Windows.Forms.Label();
            this.btn_zurueckcharacter_home = new System.Windows.Forms.Button();
            this.lbl_punktelbl = new System.Windows.Forms.Label();
            this.lbl_spielernamelbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_character)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_character
            // 
            this.pb_character.Location = new System.Drawing.Point(512, 45);
            this.pb_character.Name = "pb_character";
            this.pb_character.Size = new System.Drawing.Size(367, 469);
            this.pb_character.TabIndex = 0;
            this.pb_character.TabStop = false;
            // 
            // lbl_spielername
            // 
            this.lbl_spielername.AutoSize = true;
            this.lbl_spielername.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spielername.Location = new System.Drawing.Point(203, 109);
            this.lbl_spielername.Name = "lbl_spielername";
            this.lbl_spielername.Size = new System.Drawing.Size(97, 18);
            this.lbl_spielername.TabIndex = 1;
            this.lbl_spielername.Text = "Spielername";
            // 
            // lbl_punkte
            // 
            this.lbl_punkte.AutoSize = true;
            this.lbl_punkte.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punkte.Location = new System.Drawing.Point(203, 169);
            this.lbl_punkte.Name = "lbl_punkte";
            this.lbl_punkte.Size = new System.Drawing.Size(56, 18);
            this.lbl_punkte.TabIndex = 2;
            this.lbl_punkte.Text = "Punkte";
            // 
            // btn_zurueckcharacter_home
            // 
            this.btn_zurueckcharacter_home.Location = new System.Drawing.Point(77, 468);
            this.btn_zurueckcharacter_home.Name = "btn_zurueckcharacter_home";
            this.btn_zurueckcharacter_home.Size = new System.Drawing.Size(279, 46);
            this.btn_zurueckcharacter_home.TabIndex = 3;
            this.btn_zurueckcharacter_home.Text = "Zurück";
            this.btn_zurueckcharacter_home.UseVisualStyleBackColor = true;
            // 
            // lbl_punktelbl
            // 
            this.lbl_punktelbl.AutoSize = true;
            this.lbl_punktelbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punktelbl.Location = new System.Drawing.Point(74, 169);
            this.lbl_punktelbl.Name = "lbl_punktelbl";
            this.lbl_punktelbl.Size = new System.Drawing.Size(60, 18);
            this.lbl_punktelbl.TabIndex = 5;
            this.lbl_punktelbl.Text = "Punkte:";
            // 
            // lbl_spielernamelbl
            // 
            this.lbl_spielernamelbl.AutoSize = true;
            this.lbl_spielernamelbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spielernamelbl.Location = new System.Drawing.Point(74, 109);
            this.lbl_spielernamelbl.Name = "lbl_spielernamelbl";
            this.lbl_spielernamelbl.Size = new System.Drawing.Size(54, 18);
            this.lbl_spielernamelbl.TabIndex = 4;
            this.lbl_spielernamelbl.Text = "Name:";
            // 
            // frm_Character
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(933, 587);
            this.Controls.Add(this.lbl_punktelbl);
            this.Controls.Add(this.lbl_spielernamelbl);
            this.Controls.Add(this.btn_zurueckcharacter_home);
            this.Controls.Add(this.lbl_punkte);
            this.Controls.Add(this.lbl_spielername);
            this.Controls.Add(this.pb_character);
            this.Name = "frm_Character";
            this.Text = "Character";
            ((System.ComponentModel.ISupportInitialize)(this.pb_character)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_character;
        private System.Windows.Forms.Label lbl_spielername;
        private System.Windows.Forms.Label lbl_punkte;
        private System.Windows.Forms.Button btn_zurueckcharacter_home;
        private System.Windows.Forms.Label lbl_punktelbl;
        private System.Windows.Forms.Label lbl_spielernamelbl;
    }
}