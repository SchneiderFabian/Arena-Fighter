﻿namespace Arena_Fighter
{
    partial class frm_register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_registrieren = new System.Windows.Forms.Button();
            this.btn_zurueck = new System.Windows.Forms.Button();
            this.lbl_registrieren = new System.Windows.Forms.Label();
            this.lbl_passwort2 = new System.Windows.Forms.Label();
            this.lbl_nutzer = new System.Windows.Forms.Label();
            this.tbx_passwort_register = new System.Windows.Forms.TextBox();
            this.tbx_benutzername_register = new System.Windows.Forms.TextBox();
            this.lbl_passwort3 = new System.Windows.Forms.Label();
            this.tbx_passwort2_register = new System.Windows.Forms.TextBox();
            this.lbl_playername = new System.Windows.Forms.Label();
            this.tbx_spielername_register = new System.Windows.Forms.TextBox();
            this.pb_char1 = new System.Windows.Forms.PictureBox();
            this.pb_char2 = new System.Windows.Forms.PictureBox();
            this.pb_char3 = new System.Windows.Forms.PictureBox();
            this.pb_char6 = new System.Windows.Forms.PictureBox();
            this.pb_char5 = new System.Windows.Forms.PictureBox();
            this.pb_char4 = new System.Windows.Forms.PictureBox();
            this.lbl_character = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char4)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_registrieren
            // 
            this.btn_registrieren.Location = new System.Drawing.Point(302, 303);
            this.btn_registrieren.Name = "btn_registrieren";
            this.btn_registrieren.Size = new System.Drawing.Size(317, 43);
            this.btn_registrieren.TabIndex = 0;
            this.btn_registrieren.Text = "Registrieren";
            this.btn_registrieren.UseVisualStyleBackColor = true;
            // 
            // btn_zurueck
            // 
            this.btn_zurueck.Location = new System.Drawing.Point(36, 303);
            this.btn_zurueck.Name = "btn_zurueck";
            this.btn_zurueck.Size = new System.Drawing.Size(199, 43);
            this.btn_zurueck.TabIndex = 1;
            this.btn_zurueck.Text = "Zurück zum Login";
            this.btn_zurueck.UseVisualStyleBackColor = true;
            // 
            // lbl_registrieren
            // 
            this.lbl_registrieren.AutoSize = true;
            this.lbl_registrieren.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbl_registrieren.Location = new System.Drawing.Point(201, 9);
            this.lbl_registrieren.Name = "lbl_registrieren";
            this.lbl_registrieren.Size = new System.Drawing.Size(132, 24);
            this.lbl_registrieren.TabIndex = 2;
            this.lbl_registrieren.Text = "Registrieren";
            // 
            // lbl_passwort2
            // 
            this.lbl_passwort2.AutoSize = true;
            this.lbl_passwort2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwort2.Location = new System.Drawing.Point(33, 128);
            this.lbl_passwort2.Name = "lbl_passwort2";
            this.lbl_passwort2.Size = new System.Drawing.Size(57, 14);
            this.lbl_passwort2.TabIndex = 8;
            this.lbl_passwort2.Text = "Passwort:";
            // 
            // lbl_nutzer
            // 
            this.lbl_nutzer.AutoSize = true;
            this.lbl_nutzer.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nutzer.Location = new System.Drawing.Point(33, 83);
            this.lbl_nutzer.Name = "lbl_nutzer";
            this.lbl_nutzer.Size = new System.Drawing.Size(80, 14);
            this.lbl_nutzer.TabIndex = 7;
            this.lbl_nutzer.Text = "Benutzername:";
            // 
            // tbx_passwort_register
            // 
            this.tbx_passwort_register.Location = new System.Drawing.Point(166, 125);
            this.tbx_passwort_register.Name = "tbx_passwort_register";
            this.tbx_passwort_register.Size = new System.Drawing.Size(206, 20);
            this.tbx_passwort_register.TabIndex = 6;
            // 
            // tbx_benutzername_register
            // 
            this.tbx_benutzername_register.Location = new System.Drawing.Point(166, 80);
            this.tbx_benutzername_register.Name = "tbx_benutzername_register";
            this.tbx_benutzername_register.Size = new System.Drawing.Size(206, 20);
            this.tbx_benutzername_register.TabIndex = 5;
            // 
            // lbl_passwort3
            // 
            this.lbl_passwort3.AutoSize = true;
            this.lbl_passwort3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwort3.Location = new System.Drawing.Point(33, 177);
            this.lbl_passwort3.Name = "lbl_passwort3";
            this.lbl_passwort3.Size = new System.Drawing.Size(110, 14);
            this.lbl_passwort3.TabIndex = 10;
            this.lbl_passwort3.Text = "Passwort bestätigen:";
            // 
            // tbx_passwort2_register
            // 
            this.tbx_passwort2_register.Location = new System.Drawing.Point(166, 174);
            this.tbx_passwort2_register.Name = "tbx_passwort2_register";
            this.tbx_passwort2_register.Size = new System.Drawing.Size(206, 20);
            this.tbx_passwort2_register.TabIndex = 9;
            // 
            // lbl_playername
            // 
            this.lbl_playername.AutoSize = true;
            this.lbl_playername.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_playername.Location = new System.Drawing.Point(450, 80);
            this.lbl_playername.Name = "lbl_playername";
            this.lbl_playername.Size = new System.Drawing.Size(69, 14);
            this.lbl_playername.TabIndex = 12;
            this.lbl_playername.Text = "Spielername:";
            // 
            // tbx_spielername_register
            // 
            this.tbx_spielername_register.Location = new System.Drawing.Point(588, 77);
            this.tbx_spielername_register.Name = "tbx_spielername_register";
            this.tbx_spielername_register.Size = new System.Drawing.Size(206, 20);
            this.tbx_spielername_register.TabIndex = 11;
            // 
            // pb_char1
            // 
            this.pb_char1.Location = new System.Drawing.Point(588, 147);
            this.pb_char1.Name = "pb_char1";
            this.pb_char1.Size = new System.Drawing.Size(53, 47);
            this.pb_char1.TabIndex = 13;
            this.pb_char1.TabStop = false;
            // 
            // pb_char2
            // 
            this.pb_char2.Location = new System.Drawing.Point(672, 147);
            this.pb_char2.Name = "pb_char2";
            this.pb_char2.Size = new System.Drawing.Size(53, 47);
            this.pb_char2.TabIndex = 14;
            this.pb_char2.TabStop = false;
            // 
            // pb_char3
            // 
            this.pb_char3.Location = new System.Drawing.Point(750, 147);
            this.pb_char3.Name = "pb_char3";
            this.pb_char3.Size = new System.Drawing.Size(53, 47);
            this.pb_char3.TabIndex = 15;
            this.pb_char3.TabStop = false;
            // 
            // pb_char6
            // 
            this.pb_char6.Location = new System.Drawing.Point(750, 208);
            this.pb_char6.Name = "pb_char6";
            this.pb_char6.Size = new System.Drawing.Size(53, 47);
            this.pb_char6.TabIndex = 18;
            this.pb_char6.TabStop = false;
            // 
            // pb_char5
            // 
            this.pb_char5.Location = new System.Drawing.Point(672, 208);
            this.pb_char5.Name = "pb_char5";
            this.pb_char5.Size = new System.Drawing.Size(53, 47);
            this.pb_char5.TabIndex = 17;
            this.pb_char5.TabStop = false;
            // 
            // pb_char4
            // 
            this.pb_char4.Location = new System.Drawing.Point(588, 208);
            this.pb_char4.Name = "pb_char4";
            this.pb_char4.Size = new System.Drawing.Size(53, 47);
            this.pb_char4.TabIndex = 16;
            this.pb_char4.TabStop = false;
            // 
            // lbl_character
            // 
            this.lbl_character.AutoSize = true;
            this.lbl_character.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_character.Location = new System.Drawing.Point(450, 131);
            this.lbl_character.Name = "lbl_character";
            this.lbl_character.Size = new System.Drawing.Size(58, 14);
            this.lbl_character.TabIndex = 19;
            this.lbl_character.Text = "Character:";
            // 
            // frm_register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(854, 358);
            this.Controls.Add(this.lbl_character);
            this.Controls.Add(this.pb_char6);
            this.Controls.Add(this.pb_char5);
            this.Controls.Add(this.pb_char4);
            this.Controls.Add(this.pb_char3);
            this.Controls.Add(this.pb_char2);
            this.Controls.Add(this.pb_char1);
            this.Controls.Add(this.lbl_playername);
            this.Controls.Add(this.tbx_spielername_register);
            this.Controls.Add(this.lbl_passwort3);
            this.Controls.Add(this.tbx_passwort2_register);
            this.Controls.Add(this.lbl_passwort2);
            this.Controls.Add(this.lbl_nutzer);
            this.Controls.Add(this.tbx_passwort_register);
            this.Controls.Add(this.tbx_benutzername_register);
            this.Controls.Add(this.lbl_registrieren);
            this.Controls.Add(this.btn_zurueck);
            this.Controls.Add(this.btn_registrieren);
            this.Name = "frm_register";
            this.Text = "Registrieren";
            this.Load += new System.EventHandler(this.frm_register_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_char1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_char4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_registrieren;
        private System.Windows.Forms.Button btn_zurueck;
        private System.Windows.Forms.Label lbl_registrieren;
        private System.Windows.Forms.Label lbl_passwort2;
        private System.Windows.Forms.Label lbl_nutzer;
        public System.Windows.Forms.TextBox tbx_passwort_register;
        public System.Windows.Forms.TextBox tbx_benutzername_register;
        private System.Windows.Forms.Label lbl_passwort3;
        public System.Windows.Forms.TextBox tbx_passwort2_register;
        private System.Windows.Forms.Label lbl_playername;
        public System.Windows.Forms.TextBox tbx_spielername_register;
        private System.Windows.Forms.PictureBox pb_char1;
        private System.Windows.Forms.PictureBox pb_char2;
        private System.Windows.Forms.PictureBox pb_char3;
        private System.Windows.Forms.PictureBox pb_char6;
        private System.Windows.Forms.PictureBox pb_char5;
        private System.Windows.Forms.PictureBox pb_char4;
        private System.Windows.Forms.Label lbl_character;
    }
}