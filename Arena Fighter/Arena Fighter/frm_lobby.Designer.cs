﻿namespace Arena_Fighter
{
    partial class frm_lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Lobby = new System.Windows.Forms.Label();
            this.btn_zuruecklobby_home = new System.Windows.Forms.Button();
            this.lbx_playersinlobby = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbl_Lobby
            // 
            this.lbl_Lobby.AutoSize = true;
            this.lbl_Lobby.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbl_Lobby.Location = new System.Drawing.Point(229, 50);
            this.lbl_Lobby.Name = "lbl_Lobby";
            this.lbl_Lobby.Size = new System.Drawing.Size(73, 24);
            this.lbl_Lobby.TabIndex = 0;
            this.lbl_Lobby.Text = "Lobby";
            // 
            // btn_zuruecklobby_home
            // 
            this.btn_zuruecklobby_home.Location = new System.Drawing.Point(90, 510);
            this.btn_zuruecklobby_home.Name = "btn_zuruecklobby_home";
            this.btn_zuruecklobby_home.Size = new System.Drawing.Size(345, 40);
            this.btn_zuruecklobby_home.TabIndex = 3;
            this.btn_zuruecklobby_home.Text = "Zurück";
            this.btn_zuruecklobby_home.UseVisualStyleBackColor = true;
            // 
            // lbx_playersinlobby
            // 
            this.lbx_playersinlobby.FormattingEnabled = true;
            this.lbx_playersinlobby.Location = new System.Drawing.Point(90, 112);
            this.lbx_playersinlobby.Name = "lbx_playersinlobby";
            this.lbx_playersinlobby.Size = new System.Drawing.Size(345, 368);
            this.lbx_playersinlobby.TabIndex = 2;
            // 
            // frm_lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(531, 584);
            this.Controls.Add(this.btn_zuruecklobby_home);
            this.Controls.Add(this.lbx_playersinlobby);
            this.Controls.Add(this.lbl_Lobby);
            this.Name = "frm_lobby";
            this.Text = "Lobby";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Lobby;
        private System.Windows.Forms.Button btn_zuruecklobby_home;
        private System.Windows.Forms.ListBox lbx_playersinlobby;
    }
}