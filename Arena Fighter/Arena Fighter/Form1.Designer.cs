﻿namespace Arena_Fighter
{
    partial class frm_login
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_benutzernamelogin = new System.Windows.Forms.TextBox();
            this.tbx_passwortlogin = new System.Windows.Forms.TextBox();
            this.btn_einloggen = new System.Windows.Forms.Button();
            this.lbl_nutzer = new System.Windows.Forms.Label();
            this.lbl_passwort = new System.Windows.Forms.Label();
            this.btn_registrieren = new System.Windows.Forms.Button();
            this.lbl_login = new System.Windows.Forms.Label();
            this.btn_debug = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbx_benutzernamelogin
            // 
            this.tbx_benutzernamelogin.Location = new System.Drawing.Point(278, 178);
            this.tbx_benutzernamelogin.Name = "tbx_benutzernamelogin";
            this.tbx_benutzernamelogin.Size = new System.Drawing.Size(206, 20);
            this.tbx_benutzernamelogin.TabIndex = 0;
            // 
            // tbx_passwortlogin
            // 
            this.tbx_passwortlogin.Location = new System.Drawing.Point(278, 300);
            this.tbx_passwortlogin.Name = "tbx_passwortlogin";
            this.tbx_passwortlogin.Size = new System.Drawing.Size(206, 20);
            this.tbx_passwortlogin.TabIndex = 1;
            // 
            // btn_einloggen
            // 
            this.btn_einloggen.Location = new System.Drawing.Point(148, 398);
            this.btn_einloggen.Name = "btn_einloggen";
            this.btn_einloggen.Size = new System.Drawing.Size(336, 56);
            this.btn_einloggen.TabIndex = 2;
            this.btn_einloggen.Text = "Einloggen";
            this.btn_einloggen.UseVisualStyleBackColor = true;
            // 
            // lbl_nutzer
            // 
            this.lbl_nutzer.AutoSize = true;
            this.lbl_nutzer.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nutzer.Location = new System.Drawing.Point(145, 181);
            this.lbl_nutzer.Name = "lbl_nutzer";
            this.lbl_nutzer.Size = new System.Drawing.Size(80, 14);
            this.lbl_nutzer.TabIndex = 3;
            this.lbl_nutzer.Text = "Benutzername:";
            // 
            // lbl_passwort
            // 
            this.lbl_passwort.AutoSize = true;
            this.lbl_passwort.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwort.Location = new System.Drawing.Point(145, 303);
            this.lbl_passwort.Name = "lbl_passwort";
            this.lbl_passwort.Size = new System.Drawing.Size(57, 14);
            this.lbl_passwort.TabIndex = 4;
            this.lbl_passwort.Text = "Passwort:";
            // 
            // btn_registrieren
            // 
            this.btn_registrieren.Location = new System.Drawing.Point(148, 485);
            this.btn_registrieren.Name = "btn_registrieren";
            this.btn_registrieren.Size = new System.Drawing.Size(336, 56);
            this.btn_registrieren.TabIndex = 5;
            this.btn_registrieren.Text = "Registrieren";
            this.btn_registrieren.UseVisualStyleBackColor = true;
            // 
            // lbl_login
            // 
            this.lbl_login.AutoSize = true;
            this.lbl_login.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_login.Location = new System.Drawing.Point(273, 78);
            this.lbl_login.Name = "lbl_login";
            this.lbl_login.Size = new System.Drawing.Size(67, 24);
            this.lbl_login.TabIndex = 6;
            this.lbl_login.Text = "Login";
            // 
            // btn_debug
            // 
            this.btn_debug.Location = new System.Drawing.Point(12, 12);
            this.btn_debug.Name = "btn_debug";
            this.btn_debug.Size = new System.Drawing.Size(127, 56);
            this.btn_debug.TabIndex = 7;
            this.btn_debug.Text = "Debug";
            this.btn_debug.UseVisualStyleBackColor = true;
            this.btn_debug.Click += new System.EventHandler(this.btn_debug_Click);
            // 
            // frm_login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(595, 583);
            this.Controls.Add(this.btn_debug);
            this.Controls.Add(this.lbl_login);
            this.Controls.Add(this.btn_registrieren);
            this.Controls.Add(this.lbl_passwort);
            this.Controls.Add(this.lbl_nutzer);
            this.Controls.Add(this.btn_einloggen);
            this.Controls.Add(this.tbx_passwortlogin);
            this.Controls.Add(this.tbx_benutzernamelogin);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frm_login";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox tbx_benutzernamelogin;
        public System.Windows.Forms.TextBox tbx_passwortlogin;
        private System.Windows.Forms.Button btn_einloggen;
        private System.Windows.Forms.Label lbl_nutzer;
        private System.Windows.Forms.Label lbl_passwort;
        private System.Windows.Forms.Button btn_registrieren;
        private System.Windows.Forms.Label lbl_login;
        private System.Windows.Forms.Button btn_debug;
    }
}

