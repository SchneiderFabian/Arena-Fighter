﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arena_Fighter
{
    class cls_Spiele
    {
        int m_bid;
        string m_benutzername;
        string m_passwort;
        string m_charaktername;
        int m_level;
        int m_sid;

        public int Bid { get => m_bid; set => m_bid = value; }
        public string Benutzername { get => m_benutzername; set => m_benutzername = value; }
        public string Passwort { get => m_passwort; set => m_passwort = value; }
        public string Charaktername { get => m_charaktername; set => m_charaktername = value; }
        public int Level { get => m_level; set => m_level = value; }
        public int Sid { get => m_sid; set => m_sid = value; }

        public cls_Spiele(int bid, string benutzername, string passwort, string charaktername, int level, int sid)
        {
            m_bid = bid;
            m_benutzername = benutzername;
            m_passwort = passwort;
            m_charaktername = charaktername;
            m_level = level;
            m_sid = sid;
        }

        public cls_Spiele(string benutzername, string passwort, string charaktername, int level)
        {
            m_benutzername = benutzername;
            m_passwort = passwort;
            m_charaktername = charaktername;
            m_level = level;
        }
    }
}
